/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.circuits;

import seriaf.poo.tema3.gates.Driver;
import seriaf.poo.tema3.gates.AndGate;
import seriaf.poo.tema3.gates.InverterGate;
import seriaf.poo.tema3.gates.XorGate;

/**
 *
 * @author ana
 */
public class ElementaryMultiplexer implements Driver {

    protected boolean gOutput = false;
    protected Driver sel = null, in0 = null, in1 = null;

    public ElementaryMultiplexer(Driver s, Driver i0, Driver i1) {
        this.sel = s;
        this.in0 = i0;
        this.in1 = i1;
    }

    @Override
    public boolean getOutput() {
        this.gOutput = (new XorGate(
                new AndGate(this.in0, new InverterGate(this.sel)),
                new AndGate(this.sel, this.in1)
        )).getOutput();
        return this.gOutput;
    }

}
