/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.circuits;

import seriaf.poo.tema3.gates.AndGate;
import seriaf.poo.tema3.gates.ConstantValue;
import seriaf.poo.tema3.gates.InverterGate;
import seriaf.poo.tema3.gates.OrGate;
import seriaf.poo.tema3.gates.XorGate;

/**
 *
 * @author ana
 */
public class Main {

    public static void main(String[] args) {
        ConstantValue t = new ConstantValue(true),
                f = new ConstantValue(false);
        ElementaryMultiplexer a = new ElementaryMultiplexer(f, f, f);
        ElementaryMultiplexer b = new ElementaryMultiplexer(f, f, t);
        ElementaryMultiplexer c = new ElementaryMultiplexer(f, t, t);
        ElementaryMultiplexer d = new ElementaryMultiplexer(f, t, f);
        ElementaryMultiplexer e = new ElementaryMultiplexer(t, f, t);
        ElementaryMultiplexer ff = new ElementaryMultiplexer(t, f, f);
        ElementaryMultiplexer g = new ElementaryMultiplexer(t, t, f);
        ElementaryMultiplexer h = new ElementaryMultiplexer(t, t, t);
        System.out.println(a.getOutput());
        System.out.println(b.getOutput());
        System.out.println(c.getOutput());
        System.out.println(d.getOutput());
        System.out.println(e.getOutput());
        System.out.println(ff.getOutput());
        System.out.println(g.getOutput());
        System.out.println(h.getOutput());

        System.out.println("\nTest AND");
        System.out.println((new AndGate(f, f)).getOutput());
        System.out.println((new AndGate(f, t)).getOutput());
        System.out.println((new AndGate(t, f)).getOutput());
        System.out.println((new AndGate(t, t)).getOutput());
        System.out.println("\nTest OR");
        System.out.println((new OrGate(f, f)).getOutput());
        System.out.println((new OrGate(f, t)).getOutput());
        System.out.println((new OrGate(t, f)).getOutput());
        System.out.println((new OrGate(t, t)).getOutput());
        System.out.println("\nTest XOR");
        System.out.println((new XorGate(f, f, a)).getOutput());
        System.out.println((new XorGate(f, t)).getOutput());
        System.out.println((new XorGate(t, f)).getOutput());
        System.out.println((new XorGate(t, t)).getOutput());
        System.out.println("\nTest NOT");
        System.out.println((new InverterGate(f)).getOutput());
        System.out.println((new InverterGate(t)).getOutput());
    }
}
