/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.gates;

/**
 *
 * @author ana
 */
public class XorGate implements Driver {

    protected boolean gOutput = true;
    protected Driver[] gInputs = null;

    public XorGate(Driver... drv) {
        if (drv.length < 2) {
            throw new IllegalArgumentException("Prea putine argumente");
        }
        this.gOutput = drv[0].getOutput();
        this.gInputs = drv;

    }

    @Override
    public boolean getOutput() {
        for (int i = 1; i < gInputs.length; i++) {
            this.gOutput = this.gOutput ^ gInputs[i].getOutput();
        }
        return this.gOutput;
    }
}
