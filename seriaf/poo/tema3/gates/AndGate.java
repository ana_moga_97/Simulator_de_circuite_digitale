/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.gates;

/**
 *
 * @author ana
 */
public class AndGate implements Driver {

    protected boolean gOutput = false;
    protected Driver[] gInputs = null;

    public AndGate(Driver... drv) {
        if (drv.length < 2) {
            throw new IllegalArgumentException("Prea putine argumente");
        }
        this.gOutput = drv[0].getOutput();
        this.gInputs = drv;

    }

    @Override
    public boolean getOutput() {
        for (Driver sd : this.gInputs) {
            this.gOutput = this.gOutput && sd.getOutput();
        }
        return this.gOutput;
    }
}
