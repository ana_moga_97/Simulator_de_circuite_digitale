/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.gates;

/**
 *
 * @author ana
 */
public class ConstantValue implements Driver {

    protected boolean gOuput = false;

    public ConstantValue(boolean arg) {
        this.gOuput = arg;
    }

    @Override
    public boolean getOutput() {
        return this.gOuput;
    }
}
