/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema3.gates;

/**
 *
 * @author ana
 */
public class InverterGate implements Driver {

    protected boolean gOutput = false;

    public InverterGate(Driver drv) {
        this.gOutput = !drv.getOutput();
    }

    @Override
    public boolean getOutput() {
        return this.gOutput;
    }

}
