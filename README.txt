Introducere

�n domeniul electronicii digitale, elementele de baz� �n construc?ia circuitelor se numesc por?i logice. Aceste por?i au una sau mai multe intr�ri (�n func?ie de tip) ?i o ie?ire. Pe fiecare intrare ?i ie?ire poate fi la un moment dat o singur� valoare logic� (1 sau 0 / true sau false). Aceste por?i logice sunt de asemenea alimentate (porturile de alimentare nu se reprezint� grafic), deci semnalul de ie?ire este "produs" de aceste por?i �n func?ie de valorile de intrare, ?i nu este "generat" efectiv din semnalele de intrare. �n englez�, the gate drives the signal. Astfel, Orice poart� logic� poate fi considerat� un Driver pentru valoarea de ie?ire. Por?ile logice care ne intereseaz� sunt reprezentate grafic ?i descrise �n tabelul de mai jos:


Tip	Simbol	Tabel de adev�r	Tip	Simbol	Tabel de adev�r
AND	AND symbol	
INTRARE	IE?IRE
A	B	A AND B
0	0	0
0	1	0
1	0	0
1	1	1
OR	OR symbol	
INTRARE	IE?IRE
A	B	A OR B
0	0	0
0	1	1
1	0	1
1	1	1
NOT	NOT symbol	
INTRARE	IE?IRE
A	NOT A
0	1
1	0
XOR
XOR symbol
INTRARE	IE?IRE
A	B	A XOR B
0	0	0
0	1	1
1	0	1
1	1	0


Observa?ii:
Poarta NOT (inversorul) are �ntotdeauna exact o intrare ?i exact o ie?ire. Valoarea de pe ie?ire este negat� fa?� de cea de pe intrare.
Celelalte por?i au cel pu?in dou� intr�ri (nu exist� teoretic limit� superioar�) ?i o ie?ire. Valoarea de pe ie?ire se calculeaz� �n func?ie de tipul por?ii. Pentru doar dou� intr�ri, ave?i tabelele de adev�r mai sus. 


Cerin?�

D�ndu-se interfa?a seriaf.poo.tema3.gates.Driver:

package seriaf.poo.tema3.gates;

public interface Driver {
    boolean getOutput();
}
Crea?i urm�toarele clase care s� implementeze aceast� interfa?�:

 seriaf.poo.tema3.gates.ConstantValue: Aceast� clas� trebuie s� aib� un constructor care s� ia ca argument o valoare de tip boolean pe care metoda getOutput() s� o �ntoarc� c�nd aceasta este apelat�.
 seriaf.poo.tema3.gates.InverterGate: Aceast� clas� trebuie s� aib� un constructor care s� ia ca argument o referin?� de tip Driver care reprezint� intrarea inversorului. Metoda getOutput() trebuie s� �ntoarc� valoarea de pe intrare, negat�, conform tabelului de mai sus.
 seriaf.poo.tema3.gates.AndGate: Aceast� clas� trebuie s� aib� un constructor care s� ia ca argument un num�r variabil de referin?e de tip Driver care reprezint� intr�rile inversorului (pentru explica?ii legate de utilizarea unui num�r variabil de argumente pentru o metod�/ constructor, pute?i urm�ri acest tutorial: http://java-demos.blogspot.ro/2012/12/java-varargs-tutorial.html). Dac� constructorul nu are minim 2 argumente de tip Driver, acesta trebuie s� arunce o excep?ie de tip IllegalArgumentException. Metoda getOutput() trebuie s� �ntoarc� valoarea calculat� a ie?irii.
 seriaf.poo.tema3.gates.OrGate: Aceast� clas� trebuie s� aib� un constructor care s� ia ca argument un num�r variabil de referin?e de tip Driver care reprezint� intr�rile inversorului (pentru explica?ii legate de utilizarea unui num�r variabil de argumente pentru o metod�/ constructor, pute?i urm�ri acest tutorial: http://java-demos.blogspot.ro/2012/12/java-varargs-tutorial.html). Dac� constructorul nu are minim 2 argumente de tip Driver, acesta trebuie s� arunce o excep?ie de tip IllegalArgumentException. Metoda getOutput() trebuie s� �ntoarc� valoarea calculat� a ie?irii.
 seriaf.poo.tema3.gates.XorGate: Aceast� clas� trebuie s� aib� un constructor care s� ia ca argument un num�r variabil de referin?e de tip Driver care reprezint� intr�rile inversorului (pentru explica?ii legate de utilizarea unui num�r variabil de argumente pentru o metod�/ constructor, pute?i urm�ri acest tutorial: http://java-demos.blogspot.ro/2012/12/java-varargs-tutorial.html). Dac� constructorul nu are minim 2 argumente de tip Driver, acesta trebuie s� arunce o excep?ie de tip IllegalArgumentException. Metoda getOutput() trebuie s� �ntoarc� valoarea calculat� a ie?irii.
 seriaf.poo.tema3.circuits.ElementaryMultiplexer: Folosind EXCLUSIV obiecte de tipul celor de mai sus, realiza?i aceast� clas� care s� simuleze circuitul desenat mai jos. Constructorul acestei clase trebuie s� aiba 3 argumente de tip Driver care reprezint� intr�rile sel, in0 ?i in1, �n aceast� ordine. Metoda getOutput() va trebui s� �ntoarc� valoarea ie?irii.


Aten?ie, pentru a rezolva cerin?ele de mai sus ?i a lua punctaj maxim la aceast� tem�, nu este necesar� scrierea unei metode main. Totu?i, dac� dori?i s� testa?i �ntr-un program clasele scrise, pute?i realiza o metod� main �n care s� instan?ia?i un obiect de tip ElementaryMultiplexer care s� ia la intrare obiecte de tip ConstantValue cu diferite valori ?i s� testa?i c� valoarea ie?irii este cea corect�.